import React from 'react';

class AddCar extends React.Component {
	
	
	render(){
		let makeOptions = this.props.makes.map(make => {
			return <option key={make} value="make">{make}</option>
		});
		return(
			<div>
				<h2>Add Car</h2>
				<form>
					<div>
						<label>Year</label><br/>
						<input type="text" />
					</div>
					<div>
						<label>Make</label><br/>
						<select ref="make">
							{makeOptions }
						</select>
					</div>
					<div>
						<label>Model</label><br/>
						<input type="text" />
					</div>
					<div>
						<label>Year</label><br/>
						<input type="button" value="Submit" />
					</div>
				</form>
			</div>
		);
	}
}

export default AddCar;