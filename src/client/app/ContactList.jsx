import React from 'react';
import Contact from './Contact.jsx';
import Request from 'superagent';
import _ from 'lodash';

//Component lifecycle docs:
//https://facebook.github.io/react/docs/react-component.html
class ContactList extends React.Component {
	constructor(){
		super();
		this.state = {
			text: "Initial Text",
			name: "Arvind",
			search: ''
		};
	}
	UpdateSearch(event) {
		// console.log(event.target.value);
		this.setState({search: event.target.value.substr(0, 20)});
	}

	componentWillMount(){
		//Called the firtst time the component is loaded right before the component is added to the page
		this.search();
	}
	componentDidMount() {
		//Called after the component has been rendered into the page
		var button = document.getElementById('button');
	}
	componentWilReceiveProps(nextProps){
		//Called when the props provided to the component are changed
	}
	componentWillUpdate(nextProps, nextState) {
		//Called when the props and/or state change
		//console.log('updating....');
		//console.log(nextState.text);
	}
	componentWillUnMount(){
		//Called when the component is removed
	}	
	clicked(){
		this.setState({text: this.refs.txtBox.value});
	}
	nameChange(name){
		this.setState({name: name});
	}
	selectMovie(){
		console.log(this.refs.movieSelector.value);
	}
	render(){
		let filteredContacts = this.props.contacts.filter(
			(contact) => {
				return contact.name.toLowerCase().indexOf(
					this.state.search.toLowerCase()) !== -1;
			}
		);

		var movies = _.map(this.state.movies, (movie) => {
      		return <li key={movie.imdbID}>{movie.Title}</li>;
    	});
    	var options = _.map(this.state.movies, (movie) => {
    		return <option key={`option_${movie.imdbID}`} value={movie.imdbID}>{movie.Title}</option>
    	});


		let filteredOptions = this.props.contacts
		return(
			<div>
			
			</div>
		)
	}
	search(query = "star"){
	    var url = `http://www.omdbapi.com?s=${query}&y=&r=json&plot=short`;
	    Request.get(url).then((response) => {
	      this.setState({
	        movies: response.body.Search,
	        total: response.body.totalResults
	      });
	    });
	}
}
export default ContactList; 