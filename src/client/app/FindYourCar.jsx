import React from 'react';
import Request from 'superagent';
import _ from 'lodash';
import FindYourCarItem from './FindYourCarItem.jsx'

class FindYourCar extends React.Component {
	constructor(){
		super();
		this.state = {
			disabled: true
		}
		this.selectYear = this.selectYear.bind(this);
		this.selectMake = this.selectMake.bind(this);
	}
	selectYear(event, disabled) {
		console.log("year is selected");
		this.setState({ disabled: false});
		//var optionValue = event.target.value;
		//console.log(optionValue)
		// alert(event.target.value)
		// this.setState({disabled: true});
	}
	selectMake(disabled){
		console.log("select make");
		// this.setState({disabled: false});
	}
	enableDropdown(){
		this.setState({ disabled: false});
	}
	// _onClick() {
 //    		console.log("clicked");
 //    	}	
	handleSubmit(event) {
		event.preventDefault();
		console.log("submit clicked");
	}
	render(){

		// console.log(this.props);
		let CarYears, carMakes, carModels, transmissionTypes, noofCylinders, bodyTypes;
		// let disabled = false;

		if(this.props){
			CarYears = this.props.carYears.map(carYear => {
				// console.log(car);
				return(
					<FindYourCarItem key={carYear.year} />
				);
			});
			carMakes = this.props.carMakes.map(carMake => {
				return (
					<FindYourCarItem key={carMake.id}  />
				);
			});
			carModels = this.props.carModels.map(carModel => {
				
				return(
					<FindYourCarItem key={carModel} />
				)
			});
			transmissionTypes = this.props.transmissionTypes.map(transmissionType => {
				return(
					<FindYourCarItem key={transmissionType.id} />
				)
			});
			noofCylinders = this.props.noofCylinders.map(noofCylinder => {
				return(
					<FindYourCarItem key={noofCylinder.id} />
				)
			});
			bodyTypes = this.props.bodyTypes.map(bodyType => {
				return(
					<FindYourCarItem key={bodyType.id} />
				)
			});
		}
		var carYear = _.map(this.props.carYears, (carYear) => {
			return <option key={carYear.year}>{carYear.year}</option>
		});
		var carMake = _.map(this.props.carMakes, (carMake) => {
    		return <option key={carMake.id} value={carMake.make}>{carMake.make}</option>
    	});

    	var carModel = _.map(this.props.carModels, (carModel) => {
    		return <option key={carModel} value={carModel}>{carModel}</option>	
    	});
		var transmissionType = _.map(this.props.transmissionTypes, (transmissionType) => {
    		return <option key={transmissionType.id} value={transmissionType.type}>{transmissionType.type}</option>
    	});
    	var noofCylinder = _.map(this.props.noofCylinders, (noofCylinder) => {
    		return <option key={noofCylinder.id} value={noofCylinder.cylinders}>{noofCylinder.cylinders}</option>
    	});
    	var bodyType = _.map(this.props.bodyTypes, (bodyType) => {
    		return <option key={bodyType.id} value={bodyType.body}>{bodyType.body}</option>
    	});

		return(
			<div>
				<div className="columns-group">
				
			      <form onSubmit={this.handleSubmit}>
						<div className="sg-Form-question row">
							<h2>Find Your Care using the Search Below</h2>
							<div className="col-md-3">Year of manufacture: </div>
							<div className="col-md-3">
								<select className="sg-Input" onChange={this.selectYear.bind(this)}>{carYear}</select>
							</div>
						</div>
						<div className="sg-Form-question row">
							<div className="col-md-3">
								Car Make
							</div>
							<div className="col-md-3">
								<select className="sg-Input" 
										onChange={this.selectMake.bind(this)}
										disabled={this.state.disabled}
										>{carMake}</select>
							</div>				
						</div>
						<div className="sg-Form-question row">
							<div className="col-md-3">
								Car Model
							</div>	
							<div className="col-md-3">
								<select className="sg-Input" disabled={this.state.disabled}>{carModel}</select>
							</div>
						</div>
						<div className="sg-Form-question row">
							<div className="col-md-3">
								Transmission Type
							</div>	
							<div className="col-md-3">
								<div className="col-md-3">
									<select className="sg-Input" >{transmissionType}</select>
								</div>
							</div>
						</div>
						<div className="sg-Form-question row">
							<div className="col-md-3">
								No of Cylinders
							</div>	
							<div className="col-md-3">
								<select className="sg-Input" >{noofCylinder}</select>
							</div>
						</div>
						<div className="sg-Form-question row">
							<div className="col-md-3">
								Body Type
							</div>	
							<div className="col-md-3">
								<select className="sg-Input" >{bodyType}</select>
							</div>
						</div>
						<div className="sg-Form-question row">
							<div className="col-md-3">
							</div>	
							<div className="col-md-3">
								<input type="submit" value="Find Your Car" className="sg-Btn sg-Btn--large sg-Btn--secondary find-your-car-btn"/>
							</div>
						</div>
						</form>
				</div>	
			</div>
		)
	}
}

export default FindYourCar;

