import React from 'react';
import Request from 'superagent';
import _ from 'lodash';

class FindYourCarItem extends React.Component {
	render(){
		return(
			<li className="cars">
				{this.props.carYear} - {this.props.carMake} - {this.props.carModel}
			</li>
		)
	}
}

export default FindYourCarItem;

