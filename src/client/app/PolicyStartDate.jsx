import React from 'react';
import Calendar from 'react-input-calendar';

class PolicyStartDate extends React.Component {
	render(){
		return(
			<div>
				<div className="row">
					<div className="col-md-12">
						<h4>Policy Start Date</h4>
						<Calendar format='DD/MM/YYYY' date='4-12-2016' /> 
					</div>
				</div>
			</div>
		)
	}
}

export default PolicyStartDate;