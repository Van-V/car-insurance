import React from 'react';

class TypeOfInsurance extends React.Component{
	render(){
		return(
			<div className="row typeofInsurance">
				<h4>Select type of insurance</h4>
				<div className="col-md-2 sg-Radio sg-Radio-btn"><button className="sg-Radio-text">Comprehensive</button></div>
				<div className="col-md-3"><button className="sg-Radio-text">Third Party Property Damage</button></div>
			</div>
		)
	}
}
export default TypeOfInsurance;

