import React from 'react';
import {render} from 'react-dom';
import TypeOfInsurance from './TypeOfInsurance.jsx';
import PolicyStartDate from './PolicyStartDate.jsx';
import FindYourCar from './FindYourCar.jsx';
import ContactList from './ContactList.jsx';
import Select from './Select.jsx';
import AddCar from './AddCar.jsx';

let contacts = [{
		id: 1,
		name: 'Arvind',
		phone: '66 666 6666'
	},{
		id: 2,
		name: 'Meghna',
		phone: '66 333 1111'
	},{
		id: 3,	
		name: 'Nachu',
		phone: '11 222 5555'
	},{
		id: 4,
		name: 'Van',
		phone: '55 555 7778'
	}]	



class App extends React.Component {
	constructor(){
		super();
		this.state={
			carYears: [],
			carMakes: [],
			carModels: []
		}
	}

	componentWillMount(){
		this.setState({carYears: ["1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978"]

		});
		this.setState({carYears: [{
				id: 1,
				year: "1971"
			},
			{
				id: 2,
				year: "1972"
			},
			{	id: 3,
				year: "1973"
			},
			{
				id: 4,
				year: "1974"
			},
			{	
				id: 5,
				year: "1975"
			},
			{
				id: 6,
				year: "1976"
			},
			{
				id: 7,
				year: "1979"
			}]

		});

		this.setState({carMakes: [{
				id: 1,
				year: "1971",
				make: "Audi" 
			},
			{
				id: 2,
				year: "1971",
				make: "BMW"
			},
			{	id: 3,
				year: "1972",
				make: "Benz"
			},
			{
				id: 4,
				year: "1973",
				make: "Audi"
			},
			{	
				id: 5,
				year: "1973",
				make: "Toyota"
			},
			{
				id: 6,
				year: "1973",
				make: "Skoda"
			},
			{
				id: 7,
				year: "1973",
				make: "Volkswagen"
			}]

		});
		this.setState({carModels: ["Accord", "Civic", "Cr-V", "Integra", "JAZZ", "Legend", "MDX", "Odyssey", "S2000"]

		});
		this.setState({transmissionTypes: [{
				id: 1,
				type: "Auto"
			},
			{
				id: 2,
				type: "manual"
			}]

		});
		this.setState({noofCylinders: [{
				id: 1,
				cylinders: "6"
			},
			{
				id: 2,
				cylinders: "T6"
			}]

		});
		this.setState({bodyTypes: [{
				id: 1,
				body: "4D Sedan"
			},
			{
				id: 2,
				body: "C/Chas"
			},
			{
				id: 3,
				body: "Utility"
			}]

		});
	}	
	componentWillReceiveProps(nextProps) {
	  if (this.props.value !== nextProps.value) {
	    this.setState({value: nextProps.value});
	  }
	}

	render(){
		return (
			<div className="content-wrapper">
				<TypeOfInsurance />
				<PolicyStartDate /> 
				<ContactList contacts={this.props.contacts}/>
				<FindYourCar 
					carYears={this.state.carYears} 
					carMakes={this.state.carMakes} 
					carModels={this.state.carModels}
					transmissionTypes={this.state.transmissionTypes}
					noofCylinders={this.state.noofCylinders} 
					bodyTypes={this.state.bodyTypes}	   />
				<Select />	
						
			</div>

		);
	}
}
render (<App contacts={contacts}/>, document.getElementById('app'));
