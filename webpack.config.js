var webpack = require('webpack');
var path = require('path');



var BUILD_DIR = path.resolve(__dirname, 'src/client/public');
var APP_DIR = path.resolve(__dirname, 'src/client/app');

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    path: BUILD_DIR,
    filename: '/suncorpbundle.js'
  },
  devServer: {
      inline: true,
      port: 3000
   },

  module : {
    loaders : [
      {
        test : /\.jsx?/,
        exclude: /node_modules/,
        loader : 'babel',
        query  :{
                presets:['react','es2015']
            }
      }
    ]
  }
};

module.exports = config;
